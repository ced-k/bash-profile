
# ⚠️  Attention aux dépendances ; des fichiers sont inclus ! (colors.sh, https://raw.github.com/jimeh/git-aware-prompt/)



# Configuration spécifique (Android Studio, etc.)

export ANT_HOME="/usr/share/ant"
export PATH=${PATH}:/Users/cedric/Dev/sdk/platform-tools:/Users/cedric/Dev/sdk/tools
export PATH=$PATH:$ANT_HOME/bin
export PATH=$PATH:~/bin/phantomjs
export PATH=$PATH:~/www/_scripts
export PATH=$PATH:/Users/cedric/Dev/sdk/build-tools/23.0.0/
export PATH=$PATH:/Applications/MAMP/Library/bin/
export PATH=$PATH:/Applications/MAMP/bin/
export PATH=$PATH:/usr/local/bin
export PATH=$PATH:/usr/bin/php



# prompt w/git (git-aware-prompt)

#export GITAWAREPROMPT=~/.bash/git-aware-prompt
#source "${GITAWAREPROMPT}/main.sh"
#export PS1="\u@\h \W \[$txtcyn\]\$git_branch\[$txtred\]\$git_dirty\[$txtrst\]\$ "

# prompt
# simple — export PS1="\[\033[36m\]\u\[\033[m\]@\[\033[32m\]\h:\[\033[33;1m\]\w\[\033[m\] \$ "
# w/git branch name — export PS1="\[\033[36m\]\u\[\033[m\]@\[\033[32m\]\h:\[\033[33;1m\]\w\[\033[m\] \[$txtcyn\]\$git_branch\[$txtwht\]\$ "
#export PS1="\[\033[36m\]cedⓚ \[\033[33;1m\]\w\[\033[m\] \[$txtcyn\]\$git_branch\[$txtwht\]\$ "



# git branch & colors v2
parse_git_branch() {  
	git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/ (\1)/'
}


# .bash directory with some files (colors, …)
export BASH_DIR=~/.bash
source "${BASH_DIR}/colors.sh"

# current user: \u@
displayname="cedⓚ";

# usually "$"
promptChar="❯"

# working directory
workdir="\w"

# prompt : ex. cedⓚ ~/www/smpf/plugins: (master) $ 
PS1="${text_light_cyan}${displayname}  \[${text_light_yellow}\]${workdir} \[${text_blink}\]\[${text_light_magenta}\]\$(parse_git_branch) \[${text_reset_all}\]${promptChar} "

export PS1



# other colors
export CLICOLOR=1
export LSCOLORS=ExFxBxDxCxegedabagacad



# edit/reload .bash_profile
alias bashedit='atom ~/.bash_profile'
alias sourcebash='source ~/.bash_profile'



# git
alias gitl='git log'
alias gits='git status'
alias gita='git add -A .'
alias gitp='git push'
gitc(){
  git commit -am "$1"
}
alias git-force-pull='git fetch --all && git reset --hard origin/master'



# blc as Google Bot Desktop (broken link checker)
blc_googlebot(){
	echo "Broken Link Checker (as Google Bot Desktop)"
	blc "$1" -efro --user-agent "Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)"
}



# aliases
alias cfall='find . -maxdepth 1 -type f | wc -l' # count files
alias cf='ls -l | grep -v ^l | wc -l' # count files 
alias ll='ls -laGFh'
alias mkd='mkdir $(date +%d-%m-%y)'
alias mvup='find . -maxdepth 1 -exec mv {} .. \;'

alias targz='tar zcvf'
alias tarbz='tar jcvf'
alias untargz='tar -zxvf'
alias untarbz='tar -xjf'
alias zip_folders='for i in */; do zip -r "${i%/}.zip" "$i"; done'

alias calc='bc'
alias curlbot='curl -L --user-agent "Googlebot/2.1 (+http://www.google.com/bot.html)" -v '
alias whereami='curl http://ipinfo.io'
alias ip='curl http://ipinfo.io'

alias flushdns='dscacheutil -flushcache & sudo killall -HUP mDNSResponder'
alias flushfonts='sudo atsutil databases –remove && sudo atsutil server -shutdown && sudo atsutil server -ping'



# web server
# NOTE: see also https://github.com/indexzero/http-server
alias server_static="http-server -a 0.0.0.0 -p 8080"

# Python Simple server
# alias server_static='python -m SimpleHTTPServer 8000'

#server_static(){
#	arg=$1
#	static_port=${arg:=8080}
#	python -m SimpleHTTPServer $static_port
#}

alias server_php='/usr/bin/open -a "/Applications/Google Chrome.app" "http://macmini.local:8080" ; php -S 0.0.0.0:8080'
#alias serve='echo "serving current directory..." && srvdir --auth="ana:demo" anamorphik-ced:.'



# mobile dev
alias weinre_local="weinre --boundHost 10.0.0.8 --httpPort 8888"
alias run_tablet='cordova run android --target=C7OKCT146725'
alias run_phone='cordova run android --target=SH38RW903916'



# desktop
alias keepass-update='/Users/cedric/bin/copy-keepass.sh'
alias display_hidden_files='defaults write com.apple.finder AppleShowAllFiles 1'
alias hide_hidden_files='defaults write com.apple.finder AppleShowAllFiles 0'
